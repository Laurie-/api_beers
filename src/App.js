import React from 'react';
import axios from 'axios';
import Beers from "./components/beers";
import Category from "./components/categories";
import logo from "./e83fe3d1-ec72-47d1-8337-edb50262d6a6_200x200.png";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Product from "./pages/product";
import Home from "./pages/home";
import Search from "./pages/search";


export default class App extends React.Component {
/*    state = {
        beers: []
    };


    componentDidMount() {
        // Requete qui renvoie un tableau avec mot clé "bieres"
        axios.get(`https://world.openfoodfacts.org/category/beers.json`)
            .then(res => {
                const beers = res.data.products;
                this.setState({beers});
            })
    }*/

    // affichage des 20 premiers resultat
    render() {
        return (
            <Router>
            <>
                <div className="bg-light container accueil">
                    <img src={logo} alt="logo"/>
                    <div className="main">
                    <Switch>
                        <Route path="/product">
                            <Product/>
                        </Route>
                        <Route path="/search">
                            <Search/>
                        </Route>
                        <Route path="/">
                            <Home/>
                        </Route>
                    </Switch>
                    </div>
                    <footer className="bg-dark mastfoot mt-auto">
                        <div className="text-light text-center pt-2">
                            <p>Copyright 2019 © Laurie</p>
                        </div>
                    </footer>
                </div>
            </>


            </Router>
        )
    }


}

/*export default App;*/


