import React from 'react';
import axios from 'axios';


export default class Beers extends React.Component {
    state = {
        beers: []
    };

    componentDidMount() {
        // Requete qui renvoie un tableau avec mot clé "bieres"
        axios.get(`https://world.openfoodfacts.org/cgi/search.pl?search_terms=biere&search_simple=1&action=process&json=1`)
            .then(res => {
                const beers = res.data.products;
                this.setState({beers});
            })
    }

    // affichage des 20 premiers resultat
    render() {
        return (
            <>
                <ul>
                    {this.state.beers.map(beer => <li>{beer.product_name}</li>)}
                </ul>
            </>
        )
    }
}