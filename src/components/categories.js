import React from 'react';
import axios from 'axios';


export default class Category extends React.Component {
    state = {
        category: []
    };

    componentDidMount() {
        // Requete qui renvoie un tableau avec suggestion de catégories avec string = bieres
        axios.get(`https://world.openfoodfacts.org/cgi/suggest.pl?lc=fr&tagtype=categories&string=bieres`)
            .then(res => {
                const category = res.data;
                this.setState({category});
            })
    }

    // affichage des 20 premiers resultat
    render() {
        return (
            <>
                <ul id="list">
                    {this.state.category.map(cat => <li>{cat}</li>)}
                </ul>
            </>
        )
    }
}