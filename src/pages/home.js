import React from 'react';
import {Link} from "react-router-dom";




export default class Home extends React.Component {

    render() {
        return (
            <div className="row menu">
                <div className="col-md text-center border-bottom border-warning pb-3">
                    <h2>Entrer un code barre ? </h2>
                    <input type="text"/>
                    <Link to="/product"className="btn btn-outline-warning ml-3" href="#" role="button">Rechercher</Link>
                </div>
                <div className="col-md text-center  border-top border-warning pt-3">
                    <h2>Faire une recherche ?</h2>
                    <Link to="/search" className="btn btn-outline-warning" href="#" role="button">Par ici !</Link>
                </div>
            </div>

        )
    }
}